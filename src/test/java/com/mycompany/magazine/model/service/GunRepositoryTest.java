package com.mycompany.magazine.model.service;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:/src/main/webapp/mvc-dispatcher.xml")

public class GunRepositoryTest extends TestCase {
	
	private final String JDBC = "jdbc:hsqldb:hsql://localhost/WhDb";
	
	private final String GUN_TITLE = "Pistolet";
	
	
	@Autowired
	private GunRepository gunRepository;

	@Before
	public void setUp() throws Exception {
		Connection conn = DriverManager.getConnection(JDBC);
    	Statement stmt = conn.createStatement();
    	//stmt.execute("CREATE TABLE Gun (GUN_TITLE varchar(20))");
//    	stmt.execute("INSERT INTO Gun VALUES ('" + GUN_TITLE + "')");
	}
	
	@After
	public void tearDown() throws Exception {
		Connection conn = DriverManager.getConnection(JDBC);
    	Statement stmt = conn.createStatement();
    	stmt.execute("TRUNCATE TABLE Gun");
	}
	
    @Test
    public void testReadAll() throws Exception {
    	Connection conn = DriverManager.getConnection(JDBC);
    	Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery("SELECT * FROM Gun");
    	int result = 0;
    	while (rs.next()) {
    		result++;
    	}
        assertEquals(result, gunRepository.readAll().size());
    }

    @Test
    public void testReadByTitle() {
        assertTrue(true);
    }
    @Test
    public void testReadBySerial() {
        assertTrue(true);
    }
}
