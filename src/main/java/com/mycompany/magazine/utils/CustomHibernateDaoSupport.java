package com.mycompany.magazine.utils;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public abstract class CustomHibernateDaoSupport extends HibernateDaoSupport {

    @Autowired
    private void setSF(SessionFactory sf) {
        setSessionFactory(sf);
    }
    
}
