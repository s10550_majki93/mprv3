package com.mycompany.magazine.utils;


/**

 * @param <Class>
 * @param <Serializable>
 */

public interface CrudRepository<Class, Serializable> {
    public void create(Class clazz);
    public Class read(Serializable id);
    public void update(Class clazz);
    public void delete(Class clazz);
}
