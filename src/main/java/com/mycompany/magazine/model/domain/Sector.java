package com.mycompany.magazine.model.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "Sector")
public class Sector implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long sectorId; 
	
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Person person;
    
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "gunId")
    private Set<Gun> guns; 

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "magazineId")
    private Magazine magazine;
    

    private int nr;

    public Sector() {
    }    
   
    
    public long getSectorId() {
		return sectorId;
	}

	public void setSectorId(long sectorId) {
		this.sectorId = sectorId;
	}

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }


	public Set<Gun> getGuns() {
		return guns;
	}


	public void setGuns(Set<Gun> guns) {
		this.guns = guns;
	}


	public Magazine getMagazine() {
		return magazine;
	}


	public void setMagazine(Magazine magazine) {
		this.magazine = magazine;
	}


	@Override
	public String toString() {
		return "Sector [sectorId=" + sectorId + ", person=" + person
				+ ", guns=" + guns + ", magazine=" + magazine + ", nr=" + nr
				+ "]";
	}
    
    
}
