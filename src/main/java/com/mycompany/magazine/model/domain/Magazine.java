package com.mycompany.magazine.model.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "Magazine")
public class Magazine implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long magazineId;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Person> persons;
    
    @OneToMany(fetch = FetchType.EAGER)
    private Set<Sector> sectors;
    
    @OneToMany(fetch = FetchType.EAGER)
    private Set<Gun> guns;

    private int powierzchnia;

    public Magazine() {
    }
    

    public int getPowierzchina() {
        return powierzchnia;
    }

    public void setPowierzchnia(int powierzchnia) {
        this.powierzchnia = powierzchnia;
    }

    public Set<Person> getPersons() {
        return persons;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }


	public long getMagazineId() {
		return magazineId;
	}


	public void setMagazineId(long magazineId) {
		this.magazineId = magazineId;
	}


	public Set<Sector> getSectors() {
		return sectors;
	}


	public void setSectors(Set<Sector> sectors) {
		this.sectors = sectors;
	}


	public Set<Gun> getGuns() {
		return guns;
	}


	public void setGuns(Set<Gun> guns) {
		this.guns = guns;
	}

	public int getPowierzchnia() {
		return powierzchnia;
	}


	@Override
	public String toString() {
		return "Magazine [magazineId=" + magazineId + ", persons=" + persons
				+ ", sectors=" + sectors + ", guns=" + guns + ", powierzchnia="
				+ powierzchnia + "]";
	}

	

}
