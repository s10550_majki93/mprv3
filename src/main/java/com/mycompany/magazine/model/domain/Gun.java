package com.mycompany.magazine.model.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "Gun")
public class Gun implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long gunId;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "magazineId")
    private Magazine magazine; 
    
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "sectorId")
    private Set<Sector> sectors; 
    private String title;
    private int serial;

    public Gun() {
    }
    
    
    public long getGunId() {
        return gunId;
    }

    public void setGunId(long gunId) {
        this.gunId = gunId;
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    
    public Magazine getMagazine() {
		return magazine;
	}


	public void setMagazine(Magazine magazine) {
		this.magazine = magazine;
	}


	public Set<Sector> getSectors() {
		return sectors;
	}


	public void setSectors(Set<Sector> sectors) {
		this.sectors = sectors;
	}


	@Override
	public String toString() {
		return "Gun [gunId=" + gunId + ", magazine=" + magazine + ", sectors="
				+ sectors + ", title=" + title + "]";
	}


	
}
