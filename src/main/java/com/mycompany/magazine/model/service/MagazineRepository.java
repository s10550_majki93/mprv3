package com.mycompany.magazine.model.service;

import com.mycompany.magazine.model.domain.Magazine;
import com.mycompany.magazine.utils.CrudRepository;

import java.util.List;

public interface MagazineRepository extends CrudRepository<Magazine, Long>{
    public List<Magazine> readAll();
    public List<Magazine> readByPowierzchnia(int powierzchnia);
}
