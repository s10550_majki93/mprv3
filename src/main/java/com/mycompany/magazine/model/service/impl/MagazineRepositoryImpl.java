package com.mycompany.magazine.model.service.impl;

import com.mycompany.magazine.model.domain.Magazine;
import com.mycompany.magazine.model.service.GunRepository;
import com.mycompany.magazine.model.service.MagazineRepository;
import com.mycompany.magazine.utils.CustomHibernateDaoSupport;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("magazineRepository")
@Transactional
public class MagazineRepositoryImpl extends CustomHibernateDaoSupport implements MagazineRepository {
    
    private final Logger log = Logger.getLogger(GunRepositoryImpl.class);
 
    @Override
    public List<Magazine> readAll() {
        return getHibernateTemplate().find("from Magazine");
    }

    @Override
    public List<Magazine> readByPowierzchnia(int powierzchnia) {
        return getHibernateTemplate().find("from Gun where powierzchnia = ?", powierzchnia);
    }

    @Override
    public void create(Magazine clazz) {
        getHibernateTemplate().save(clazz);
    }

    @Override
    public Magazine read(Long id) {
        return getHibernateTemplate().get(Magazine.class, id);
    }

    @Override
    public void update(Magazine clazz) {
        getHibernateTemplate().update(clazz);
    }

    @Override
    public void delete(Magazine clazz) {
        getHibernateTemplate().delete(clazz);
    }
}
