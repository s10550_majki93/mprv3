package com.mycompany.magazine.model.service;

import com.mycompany.magazine.model.domain.Sector;
import com.mycompany.magazine.utils.CrudRepository;

import java.util.List;

public interface SectorRepository extends CrudRepository<Sector, Long>{
    public List<Sector> readAll();
    public List<Sector> readByNr(int nr);
}
