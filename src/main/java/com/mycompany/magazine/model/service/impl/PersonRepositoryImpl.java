/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.magazine.model.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.magazine.model.domain.Gun;
import com.mycompany.magazine.model.domain.Person;
import com.mycompany.magazine.model.service.PersonRepository;
import com.mycompany.magazine.utils.CustomHibernateDaoSupport;

@Service("personRepository")
@Transactional
public class PersonRepositoryImpl extends CustomHibernateDaoSupport implements PersonRepository {

    private final Logger log = Logger.getLogger(PersonRepositoryImpl.class);
    
    @Override
    public List<Person> readByGun(Gun gun) {
        return getHibernateTemplate().find("from Person a join a.guns b where b.gunId = ?", gun.getGunId());
    }

    @Override
    public List<Person> readAll() {
        return getHibernateTemplate().find("from Person");
    }

    @Override
    public void create(Person clazz) {
        getHibernateTemplate().save(clazz);
        log.info("Dodano osobe: " + clazz);
    }

    @Override
    public Person read(Long id) {
        Person result = (Person) getHibernateTemplate().get(Person.class, id);
        log.info("Wczytano osobe: " + result);
        return result;
    }

    @Override
    public void update(Person clazz) {
        getHibernateTemplate().update(clazz);
        log.info("Zaktualizowano osobe: " + clazz);
    }

    @Override
    public void delete(Person clazz) {
        getHibernateTemplate().delete(clazz);
        log.info("Usunięto osobe: " + clazz);
    }
   
}
