package com.mycompany.magazine.model.service;

import com.mycompany.magazine.model.domain.Gun;
import com.mycompany.magazine.model.domain.Person;
import com.mycompany.magazine.utils.CrudRepository;

import java.util.List;

public interface PersonRepository extends CrudRepository<Person, Long> {
    public List<Person> readByGun(Gun gun);
    public List<Person> readAll();
}
