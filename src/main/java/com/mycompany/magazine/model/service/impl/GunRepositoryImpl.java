package com.mycompany.magazine.model.service.impl;

import com.mycompany.magazine.model.domain.Gun;
import com.mycompany.magazine.model.service.GunRepository;
import com.mycompany.magazine.utils.CustomHibernateDaoSupport;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("gunRepository")
@Transactional
public class GunRepositoryImpl extends CustomHibernateDaoSupport implements GunRepository {
    
    private final Logger log = Logger.getLogger(GunRepositoryImpl.class);

    @Override
    public List<Gun> readAll() {
        return getHibernateTemplate().find("from Gun");
    }

    @Override
    public List<Gun> readByTitle(String title) {
        return getHibernateTemplate().find("from Gun where title = ?", title);
    }

    @Override
    public List<Gun> readBySerial(long serial) {
        return getHibernateTemplate().find("from Gun where serial = ?", serial);
    }
    

    @Override
	public void create(Gun clazz) {
		getHibernateTemplate().save(clazz);	
	}
    
    @Override
    public Gun read(Long id) {
        return getHibernateTemplate().get(Gun.class, id);
    }

    @Override
    public void update(Gun clazz) {
        getHibernateTemplate().update(clazz);
    }

    @Override
    public void delete(Gun clazz) {
        getHibernateTemplate().delete(clazz);
    }

}
