package com.mycompany.magazine.model.service;

import com.mycompany.magazine.model.domain.Gun;
import com.mycompany.magazine.utils.CrudRepository;

import java.util.List;

public interface GunRepository extends CrudRepository<Gun, Long>{
    public List<Gun> readAll();
    public List<Gun> readByTitle(String title);
    public List<Gun> readBySerial(long serial);
}
