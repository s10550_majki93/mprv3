package com.mycompany.magazine.controller;

import java.util.HashSet;
import java.util.Set;

import com.mycompany.magazine.model.domain.Gun;
import com.mycompany.magazine.model.domain.Magazine;
import com.mycompany.magazine.model.domain.Person;
import com.mycompany.magazine.model.domain.Sector;
import com.mycompany.magazine.model.service.GunRepository;
import com.mycompany.magazine.model.service.PersonRepository;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/gun")
public class GunController {

	private final Logger log = Logger.getLogger(GunController.class);

	@Autowired
	private GunRepository gunRepository;
	
	@Autowired
	private PersonRepository personRepository;

	@RequestMapping("/")
	public String test() {
		Gun g1 = new Gun();
		Person p1 = new Person();
		p1.setSurname("Franek");
		p1.setName("Kowalski");
		Person p2 = new Person();
		p2.setSurname("Jan");
		p2.setName("Kowalski");
		Set<Sector> sectors = new HashSet<Sector>();
		Magazine magazine = new Magazine(); // dla testu
		Set<Person> persons = new HashSet<Person>();
		persons.add(p1);
		persons.add(p2);
		magazine.setPersons(persons); 
		Sector sector = new Sector();
		sector.setNr(1);
		sector.setPerson(p1);
		sectors.add(sector);
		g1.setTitle("Pistolet");
		g1.setSectors(sectors);
		g1.setMagazine(magazine);
		g1.setSerial(1234);
		gunRepository.create(g1);
		return "redirect:/gun/show";
	}

	@RequestMapping("/show")
	public String show(ModelMap model) {
		model.addAttribute("guns", gunRepository.readAll());
		return "show";
	}

	@RequestMapping({ "/json", "/json/" })
	public @ResponseBody
	String showAllGunsJson() {
		JSONArray gunArray = new JSONArray();
		for (Gun gun : gunRepository.readAll()) {
			JSONObject gunJSON = new JSONObject();
			gunJSON.put("gunId", gun.getGunId());
			gunJSON.put("title", gun.getTitle());
			gunJSON.put("serial", gun.getSerial());
			JSONObject personJSON = new JSONObject();
			for (Person person : gun.getMagazine().getPersons()) {
				personJSON.put("name", person.getName());
				personJSON.put("surname", person.getSurname());
			}
			gunJSON.put("persons", personJSON); // 
			gunArray.put(gunJSON);
		}
		return gunArray.toString();
	}

	@RequestMapping({ "/json/title/{title}", "/json/title/{title}/" })
	public @ResponseBody
	String showAllGunsByTitleJson(@PathVariable("title") String title) {
		JSONArray gunArray = new JSONArray();
		for (Gun gun : gunRepository.readByTitle(title)) {
			JSONObject gunJSON = new JSONObject();
			gunJSON.put("gunId", gun.getGunId());
			gunJSON.put("title", gun.getTitle());
			gunJSON.put("serial", gun.getSerial());
			JSONObject personJSON = new JSONObject();
			for (Person person : gun.getMagazine().getPersons()) {
				personJSON.put("name", person.getName());
				personJSON.put("surname", person.getSurname());
			}
			gunJSON.put("persons", personJSON);
			gunArray.put(gunJSON);
		}
		return gunArray.toString();
	}

	@RequestMapping({ "/json/serial/{serial}", "/json/serial/{serial}/" })
	public @ResponseBody
	String showAllGunsBySerialJson(@PathVariable("serial") int serial) {
		JSONArray gunArray = new JSONArray();
		for (Gun gun : gunRepository.readBySerial(serial)) {
			JSONObject gunJSON = new JSONObject();
			gunJSON.put("gunId", gun.getGunId());
			gunJSON.put("title", gun.getTitle());
			gunJSON.put("serial", gun.getSerial());
			JSONObject personJSON = new JSONObject();
			for (Person person : gun.getMagazine().getPersons()) {
				personJSON.put("name", person.getName());
				personJSON.put("surname", person.getSurname());
			}
			gunJSON.put("persons", personJSON);
			gunArray.put(gunJSON);
		}
		return gunArray.toString();
	}

}
