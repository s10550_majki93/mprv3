<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>


<html>
<head>
    <meta charset="utf-8">
    <title>Courier</title> 

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span8 offset2">
            <c:if test="${!empty guns}">
                <h3>Guns</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Surname & Last Name</th>
                        <th>Sector</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${guns}" var="gun">
                        <tr>
                            <td>${gun.title}</td>
                            <td>
                            <c:forEach items="${gun.sectors}" var="sector">
                            	${sector.person.name}, ${sector.person.surname}
                            </c:forEach>
                            </td>
                            <td>
                            	<c:forEach items="${gun.sectors}" var="sector">
                            	${sector.sectorId} 
                            	</c:forEach>
                            </td>
                            <td>
                                <a href="json/title/${gun.title}">JSON</a>
                                <a href="delete/${gun.title}">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>
